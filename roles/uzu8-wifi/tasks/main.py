from pyplays import Tasks

with Tasks() as t:
    t.package(name="hostapd", state="latest")
    t.copy(dest="/etc/hostapd/wlp8s0.conf", mode=0o700, content="""
interface=wlp8s0
bridge=br0
hw_mode=g
ieee80211n=1
wmm_enabled=1
# Autoselect doesn't work, so just pick 1
channel=1
ieee80211d=1
country_code=CZ

ssid=ggu_aux
# WPA-2
auth_algs=1
wpa=2
wpa_key_mgmt=WPA-PSK
rsn_pairwise=CCMP
wpa_passphrase={{ lookup('ini', 'ggu_aux file=secrets/passwords.ini section=uzu8-wifi') }}
disassoc_low_ack=0
""")
    t.service(name="hostapd@wlp8s0", state="restarted", enabled=True)
