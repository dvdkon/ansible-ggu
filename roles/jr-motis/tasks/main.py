from textwrap import dedent
from pyplays import Tasks

with Tasks() as t:
    t.package(name=["git", "cmake", "ninja-build", "build-essential",
                    "curl", "unzip"])
    t.get_url(url="https://github.com/elm-lang/elm-platform/releases/download/0.18.0-exp/elm-platform-linux-64bit.tar.gz",
        dest="/root/elm.tar.gz")
    t.file(path="/opt/elm", state="directory")
    t.unarchive(src="/root/elm.tar.gz", dest="/opt/elm", remote_src=True)

    t.git(
        repo="https://github.com/motis-project/motis",
        dest="/opt/motis",
        force=True)
    # Download deps via HTTPS, not SSH
    t.replace(path="/opt/motis/cmake/pkg.cmake",
        regexp=r"\$\{pkg-bin\} -l",
        replace="${pkg-bin} -l -h")
    t.file(path="/opt/motis/build", state="directory")
    t.stat(_register="buildfile_stat", path="/opt/motis/build/build.ninja")
    t.command(cmd="cmake -DCMAKE_BUILD_TYPE=Release -GNinja ..",
        chdir="/opt/motis/build",
        _when="not buildfile_stat.stat.exists")
    t.command(cmd="ninja", chdir="/opt/motis/build")
    t.command(cmd="/opt/elm/elm-make src/Main.elm --output elm.js --yes",
              chdir="/opt/motis/ui/web")

    t.file(path="/ssd_data/motis/input", state="directory")
    t.file(path="/ssd_data/motis/data", state="directory")
    t.copy(dest="/ssd_data/motis/input/config.ini",
        content=dedent("""\
        modules=nigiri
        modules=intermodal
        # Sadly not compatible with transfers module, which needs ppr
        #modules=valhalla
        # Trying new osr module for routing
        #modules=ppr
        modules=osr
        modules=transfers
        modules=tiles
        modules=address

        [server]
        static_path=/opt/motis/ui/web

        [nigiri]
        first_day=TODAY
        num_days=30
        default_timezone=Europe/Prague
        lookup=true
        railviz=true
        guesser=true
        routing=true
        link_stop_distance=0

        [intermodal]
        router=nigiri

        [import]
        paths=schedule-cz-jdf:/ssd_data/motis/input/JDF_merged_GTFS
        paths=schedule-cz-czptt:/ssd_data/motis/input/CZPTT_GTFS
        paths=osm:/shareddata/osmdata/europe/czech-republic/data.pbf

        [tiles]
        profile=/opt/motis/deps/tiles/profile/background.lua

        [ppr]
        profile=/opt/motis/deps/ppr/profiles/default.json
        """))

    t.copy(dest="/usr/local/bin/update_data.sh", mode=0o755,
        content=dedent("""\
        #!/bin/sh
        mkdir_empty() {
            [ -e $1 ] && rm -r $1
            mkdir $1
        }

        cd /ssd_data/motis/input

        curl https://data.jr.ggu.cz/results/latest/JDF_merged_GTFS.zip -O
        mkdir_empty JDF_merged_GTFS
        unzip -d JDF_merged_GTFS JDF_merged_GTFS.zip

        curl https://data.jr.ggu.cz/results/latest/CZPTT_GTFS.zip -O
        mkdir_empty CZPTT_GTFS
        unzip -d CZPTT_GTFS CZPTT_GTFS.zip

        systemctl restart motis
        """))

    t.copy(dest="/etc/systemd/system/motis.service",
        content=dedent("""\
        [Service]
        ExecStart=/opt/motis/build/motis -c /ssd_data/motis/input/config.ini
        SupplementaryGroups=shareddata
        WorkingDirectory=/ssd_data/motis

        [Install]
        WantedBy=multi-user.target
        """))
    t.copy(dest="/etc/systemd/system/update-data.service",
        content=dedent("""\
        [Service]
        Type=oneshot
        ExecStart=/usr/local/bin/update_data.sh
        SupplementaryGroups=shareddata
        """))
    t.copy(dest="/etc/systemd/system/update-data.timer",
        content=dedent("""\
        [Timer]
        OnCalendar=*-*-* 01:00

        [Install]
        WantedBy=timers.target
        """))
    t.systemd(daemon_reload=True)
    t.service(name="update-data.timer", state="started", enabled=True)
    t.service(name="motis.service", enabled=True)
    t.service(name="update-data.service", state="started")
