#!/usr/bin/env python3
import asyncio
import datetime as dt
import kasa
import kasa.exceptions
import msgspec
from nicegui import ui, app
from anyio import Path
from typing import cast

# CLASSES

class State(msgspec.Struct):
    online: bool = False
    wakeup_active: bool = False
    wakeup_time: dt.time = dt.time(8, 00)
    rampup_time: dt.timedelta = dt.timedelta(minutes=15)
    manual_on: bool = True
    manual_cct: int = 2700
    manual_brightness: int = 100

# CONFIG

statefile = Path("state.msgpack")

# GLOBAL STATE

lightbulb = kasa.SmartBulb("192.168.8.106")
state = State()

# BACKEND CODE

async def save_state():
    await statefile.write_bytes(msgspec.msgpack.encode(state))

async def load_state():
    global state
    # We can't assign the state, since then bindings would break
    newstate = msgspec.msgpack.decode(await statefile.read_bytes(), type=State)
    for field in msgspec.structs.fields(State):
        setattr(state, field.name, getattr(newstate, field.name))

def wakeup_datetime():
    now = dt.datetime.now()
    wkdt = dt.datetime.combine(now.date(), state.wakeup_time)
    if wkdt < now - dt.timedelta(hours=1):
        # Wakeup time is the next day
        wkdt += dt.timedelta(days=1)
    return wkdt

async def adjust_led():
    await lightbulb.update()
    if not state.wakeup_active:
        # Manual control
        if not state.manual_on:
            await lightbulb.turn_off()
        else:
            await lightbulb.set_color_temp(state.manual_cct)
            await lightbulb.set_brightness(state.manual_brightness)
            await lightbulb.turn_on()
    else:
        now = dt.datetime.now()
        start = wakeup_datetime() - state.rampup_time
        if now < start:
            await lightbulb.turn_off()
            return
        since_start = (dt.datetime.now() - start).total_seconds()
        progress = since_start / state.rampup_time.total_seconds()
        await lightbulb.set_brightness(int(progress**2 * 100), transition=10000)
        await lightbulb.set_color_temp(int(progress**3 * (6500 - 2700)) + 2700,
                                       transition=10000)
        await lightbulb.turn_on()

        if progress > 1:
            # We've finished the ramp-up, switch to manual operation now
            state.wakeup_active = False

async def try_adjust_led():
    # Could be offline
    try:
        await adjust_led()
        state.online = True
    except Exception as e:
        state.online = False

async def adjustment_loop():
    while True:
        await try_adjust_led()
        await asyncio.sleep(30)

async def update_loop():
    while True:
        try:
            await lightbulb.update()
            state.online = True
        except Exception: state.online = False
        await asyncio.sleep(10)

async def save_state_loop():
    # This is just a precaution against cold shutdowns
    while True:
        await save_state()
        await asyncio.sleep(60)

async def startup():
    try:
        await load_state()
    except:
        print("Could not load state, using defaults")

    # Get initial values from current state, if bulb is responding
    try:
        await lightbulb.update()
        ls = await lightbulb.get_light_state()
        state.manual_on = bool(ls["on_off"])
        state.manual_brightness = cast(int, ls["brightness"])
        state.manual_cct = cast(int, ls["color_temp"])
    except: pass

    asyncio.create_task(update_loop())
    asyncio.create_task(adjustment_loop())
    asyncio.create_task(save_state_loop())
app.on_startup(startup)
app.on_shutdown(save_state)

## GUI CODE

ui.label("Lightbulb is not connected!") \
    .classes("bg-red-200 px-6 py-2 rounded-md shadow-lg text-lg") \
    .bind_visibility_from(state, "online", backward=lambda o: not o)

with ui.row(wrap=False).classes("items-center"):
    ui.label("Mode:")
    ui.toggle({False: "Manual", True: "Wakeup"}) \
        .bind_value(state, "wakeup_active")

with ui.column() \
        .bind_visibility_from(state, "wakeup_active",
                              backward=lambda a: not a):
    ui.label("Brightness:")
    ui.slider(min=0, max=100).bind_value(state, "manual_brightness") \
        .classes("w-64")
    ui.label("Temperature:")
    ui.slider(min=2700, max=6500).bind_value(state, "manual_cct") \
        .classes("w-64")

    ui.button("Set", on_click=try_adjust_led)

wakeup_time_str = state.wakeup_time.strftime("%H:%M")
def wakeup_time_ok(s):
    try:
        dt.datetime.strptime(s, "%H:%M")
    except Exception:
        return False
    return True
async def set_wakeup_time():
    try:
        state.wakeup_time = \
            dt.datetime.strptime(wakeup_time_str, "%H:%M").time()
        await try_adjust_led()
    except Exception: pass

with ui.column() \
        .bind_visibility_from(state, "wakeup_active"):
    ui.label("Wakeup time:")
    ui.input(placeholder="HH:MM",
             validation={"Invalid time": wakeup_time_ok}) \
        .bind_value(globals(), "wakeup_time_str")
    ui.label("Ramp time")
    ui.number(placeholder="N minutes") \
        .bind_value(state, "rampup_time",
                    backward=lambda t: t.total_seconds() / 60,
                    forward=lambda m: dt.timedelta(minutes=m or 15))

    ui.button("Set", on_click=lambda: set_wakeup_time())

ui.run(show=False, dark=None, host="0.0.0.0", port=80)
