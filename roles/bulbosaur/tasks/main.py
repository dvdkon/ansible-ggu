from pyplays import Tasks

with Tasks() as t:
    t.package(name=["python3-pip", "python3-virtualenv"], state="latest")
    t.pip(virtualenv="/opt/bulbosaur", name=["python-kasa", "nicegui" ,"msgspec"])
    t.copy(src="bulbosaur.py", dest="/opt/bulbosaur/main.py")

    t.task("community.general.timezone", name="Europe/Prague")
    t.copy(dest="/etc/systemd/system/bulbosaur.service",
        content="""
        [Service]
        ExecStart=/opt/bulbosaur/bin/python3 /opt/bulbosaur/main.py
        WorkingDirectory=/opt/bulbosaur
        [Install]
        WantedBy=multi-user.target
        """)
    t.systemd(daemon_reload=True, name="bulbosaur", state="restarted", enabled=True)
