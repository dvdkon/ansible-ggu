from pyplays import Tasks, Args

args = Args(vars, ["users"])

version = "0.7.13"

with Tasks() as t:
    t.import_role(name="pam_ldap")

    t.package(name=["jq"], state="latest")

    t.stat(_register="kavita_stat", path="/opt/kavita")
    t.get_url(
        url=f"https://github.com/Kareadita/Kavita/releases/download/v{version}/kavita-linux-x64.tar.gz",
        dest="/root/kavita.tar.gz",
        _when="not kavita_stat.stat.exists")
    t.command(argv=["tar", "--no-same-owner", "-xf", "kavita.tar.gz"],
        chdir="/root",
        _when="not kavita_stat.stat.exists")
    t.command(argv=["mv", "/root/Kavita", "/opt/kavita"],
        _when="not kavita_stat.stat.exists")
    t.file(path="/opt/kavita/Kavita", mode=0o755)

    for cfg in args.users:
        user = cfg["username"]
        t.command(_register="uid_cmd",
            argv=["id", "-u", user])
        t.file(path=f"/data/{user}", state="directory", owner=user)
        t.file(path=f"/data/{user}/wwwroot",
            src="/opt/kavita/wwwroot",
            state="link")
        t.file(path=f"/data/{user}/config", state="directory", owner=user)
        t.copy(dest=f"/data/{user}/config/appsettings.json",
            # TokenKey will be replaced when the app is run
            # TODO: Keep it for session permanence?
            content="""
            {
              "ConnectionStrings": {
                "DefaultConnection": "Data source=kavita.db"
              },
              "TokenKey": "super secret unguessable key",
              "Logging": {
                "LogLevel": {
                  "Default": "Information",
                  "Microsoft": "Error",
                  "Microsoft.Hosting.Lifetime": "Error",
                  "Hangfire": "Error",
                    "Microsoft.AspNetCore.Hosting.Internal.WebHost": "Error"
                }
              },
              "Port": {{10000 + (uid_cmd.stdout|int)}}
            }
            """)
        t.copy(
            dest=f"/etc/systemd/system/kavita@{user}.service",
            content=f"""
            [Unit]
            Description=Kavita instance for {user}
            After=nslcd.service

            [Service]
            User={user}
            ExecStart=/opt/kavita/Kavita
            WorkingDirectory=/data/{user}

            [Install]
            WantedBy=multi-user.target
            """)

    t.systemd(daemon_reload=True)
    
    for cfg in args.users:
        t.service(name=f"kavita@{cfg['username']}", state="restarted", enabled=True)
