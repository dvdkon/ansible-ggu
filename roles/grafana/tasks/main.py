from pyplays import Tasks, Block

prometheus_version = "2.38.0"

with Tasks() as t:
    # The distro does have a Grafana package, we use the repo to get the latest
    # version
    gpg_url = "https://packages.grafana.com/gpg.key"
    # https://github.com/grafana/grafana/issues/55962
    t.package(name="crypto-policies-scripts", state="latest")
    t.command(argv=["update-crypto-policies", "--set", "DEFAULT:SHA1"])
    t.yum_repository(
        name="grafana-official",
        description="Official Grafana repo",
        baseurl="https://packages.grafana.com/oss/rpm",
        gpgkey=gpg_url,
        repo_gpgcheck=True,
        gpgcheck=True,
        sslverify=True,
        sslcacert="/etc/pki/tls/certs/ca-bundle.crt")

    t.package(name=["tar", "grafana", "postgresql-server", "loki"], state="latest")

    # PostgreSQL management database
    t.file(path="/ssd_data", mode=0o755)
    t.file(path="/ssd_data/pg", state="directory", owner="postgres")
    t.file(path="/ssd_data/pg/data", state="directory", owner="postgres")
    t.file(path="/ssd_data/pg/sock", state="directory", owner="postgres")
    t.stat(_register="pg_stat",
        path="/etc/systemd/system/postgresql@grafana.service.d")
    t.command(argv=["postgresql-new-systemd-unit", "--unit=postgresql@grafana", "--datadir=/ssd_data/pg/data"],
        _when="not pg_stat.stat.exists")
    t.command(argv=["postgresql-setup", "--initdb", "--unit", "postgresql@grafana", "--port", "5432"],
        _when="not pg_stat.stat.exists")
    t.lineinfile(path="/ssd_data/pg/data/postgresql.conf",
        regexp="^#?unix_socket_directories *=",
        line="unix_socket_directories = '/ssd_data/pg/sock'")
    t.service(name="postgresql@grafana", state="restarted", enabled=True)
    t.command(argv=["createuser", "-h", "/ssd_data/pg/sock", "grafana"],
        become=True, become_user="postgres",
        _when="not pg_stat.stat.exists")

    # Prometheus
    t.shell("Check Prometheus version",
        "/opt/prometheus/prometheus --version | sed -nEe 's/.* version ([^ ]*).*/\\1/p'",
        _register="prometheus_ver")
    with Block(t, _when=f"prometheus_ver.stdout != '{prometheus_version}'") as b:
        b.get_url(
            url=f"https://github.com/prometheus/prometheus/releases/download/v2.38.0/prometheus-{prometheus_version}.linux-amd64.tar.gz",
            dest="/root/prometheus.tar.gz")
        b.unarchive(
            src="/root/prometheus.tar.gz",
            dest="/root",
            remote_src=True)
        b.file(path="/opt/prometheus", state="absent")
        b.shell(cmd="mv /root/prometheus-* /opt/prometheus")
        b.file(path="/root/prometheus.tar.gz", state="absent")

    # Copy LXD certs
    t.file(path="/etc/prometheus/certs", state="directory", owner="prometheus", mode=0o700)
    # TODO: Find a way to work when not running on saturn
    t.copy(src="/var/lib/lxd/server.crt", dest="/etc/prometheus/certs/lxd_server.crt")
    t.copy(src="secrets/lxd_saturn_metrics.crt", dest="/etc/prometheus/certs/")
    t.copy(src="secrets/lxd_saturn_metrics.key", dest="/etc/prometheus/certs/")

    t.copy(dest="/opt/prometheus/prometheus.yml",
        content="""
global:
  scrape_interval: 1m
  evaluation_interval: 1m

scrape_configs:
  - job_name: prometheus
    static_configs:
      - targets: ["127.0.0.1:9090"]

  - job_name: lxd
    metrics_path: /1.0/metrics
    scheme: https
    static_configs:
      - targets: ["{{ hostvars['saturn'].ip }}:4863"]
    tls_config:
      ca_file: /etc/prometheus/certs/lxd_server.crt
      cert_file: /etc/prometheus/certs/lxd_saturn_metrics.crt
      key_file: /etc/prometheus/certs/lxd_saturn_metrics.key
      # Must be kept in sync with ca_file SAN!
      server_name: "saturn"
        """)
    t.user(uid=800, name="prometheus")
    t.file(path="/data/prometheus", state="directory", owner="prometheus", mode=0o700)
    t.copy(dest="/etc/systemd/system/prometheus.service",
        content="""
        [Service]
        User=prometheus
        ExecStart=/opt/prometheus/prometheus \
            --config.file=/opt/prometheus/prometheus.yml \
            --storage.tsdb.path=/data/prometheus \
            --web.listen-address="127.0.0.1:9090"

        [Install]
        WantedBy=multi-user.target
        """)

    # Loki
    t.file(path="/data/loki/chunks", state="directory", owner="loki", mode=0o700)
    t.file(path="/ssd_data/loki/wal", state="directory", owner="loki", mode=0o700)
    t.file(path="/ssd_data/loki/index", state="directory", owner="loki", mode=0o700)
    t.file(path="/ssd_data/loki/rules", state="directory", owner="loki", mode=0o700)
    t.copy(dest="/etc/loki/config.yml",
        content="""
auth_enabled: false

server:
  http_listen_address: 127.0.0.1
  http_listen_port: 3100

ingester:
  lifecycler:
    address: 127.0.0.1
    ring:
      kvstore:
        store: inmemory
      replication_factor: 1
    final_sleep: 0s
  wal:
    dir: /ssd_data/loki/wal
  chunk_idle_period: 5m
  chunk_retain_period: 30s

schema_config:
  configs:
  - from: 2020-05-15
    store: boltdb
    object_store: filesystem
    schema: v11
    index:
      prefix: index_
      period: 168h

storage_config:
  boltdb:
    directory: /ssd_data/loki/index

  filesystem:
    directory: /data/loki/chunks

limits_config:
  enforce_metric_name: false
  reject_old_samples: true
  reject_old_samples_max_age: 168h
""")


    # Grafana config
    t.copy(dest="/etc/grafana/grafana.ini",
        content="""
[server]
http_port = 3000
domain = grafana.ggu.cz
root_url = https://grafana.ggu.cz/

[database]
type = postgres
host = /ssd_data/pg/sock
name = grafana
user = grafana

[security]
disable_initial_admin_creation = true

[auth.ldap]
enabled = true

[log]
mode = console
level = info
""")

    t.copy(dest="/etc/grafana/ldap.toml",
        owner="grafana",
        mode=0o600,
        content='''
[[servers]]
host = "sidss.local.ggu.cz"
port = 636
use_ssl = true
bind_dn = "uid=ldap-svc-grafana,ou=users,dc=ggu,dc=cz"
bind_password = "{{ lookup('ini', 'grafana file=secrets/passwords.ini section=ldap_sw') }}"
search_base_dns = ["dc=ggu,dc=cz,pgm=\\"grafana.admin;grafana-admin;6000\\""]
search_filter = "(&(permissions=grafana)(uid=%s))"

[servers.attributes]
member_of = "memberOf"
name = "cn"
username = "uid"

[[servers.group_mappings]]
group_dn = "cn=grafana-admin,ou=pgms,dc=ggu,dc=cz"
org_role = "Admin"
grafana_admin = true

[[servers.group_mappings]]
group_dn = "*"
org_role = "Viewer"
''')

    # Grafana provisioning (data sources)
    t.copy(dest="/etc/grafana/provisioning/datasources/prometheus.yaml",
        content='''
apiVersion: 1
datasources:
  - name: Prometheus
    type: prometheus
    access: proxy
    url: http://127.0.0.1:9090
''')
    t.copy(dest="/etc/grafana/provisioning/datasources/loki.yaml",
        content='''
apiVersion: 1
datasources:
  - name: Loki
    type: loki
    access: proxy
    url: http://127.0.0.1:3100
''')

    t.systemd(daemon_reload=True)
    t.service(name="prometheus", state="restarted", enabled=True)
    t.service(name="loki", state="restarted", enabled=True)
    t.service(name="grafana-server", state="restarted", enabled=True)
