from pyplays import Tasks
from textwrap import dedent

with Tasks() as t:
    t.task("community.general.timezone", name="Europe/Prague")

    t.apt_key(
            url="https://deb.nodesource.com/gpgkey/nodesource.gpg.key",
            state="present")
    t.apt_repository("Add NodeSource repo",
            repo="deb https://deb.nodesource.com/node_20.x {{ansible_distribution_release}} main",
            state="present")   

    t.package(name=["nodejs",
        "postgresql-all",
        "redis-server"],
              state="latest")
    t.command(cmd="corepack enable")  # Get yarn

    t.service(name="postgresql", state="stopped")
    t.shell(cmd="[ -e /ssd_data/postgresql ] || mv /var/lib/postgresql/15/main/ /ssd_data/postgresql")
    t.file(state="link", src="/ssd_data/postgresql", dest="/var/lib/postgresql/15/main")
    t.service(name="postgresql", state="started")

    t.shell(cmd="/usr/lib/postgresql/15/bin/createuser root || true", _become=True, _become_user="postgres")
    t.shell(cmd="/usr/lib/postgresql/15/bin/createdb root || true", _become=True, _become_user="postgres")
    t.shell(cmd="psql root -c 'GRANT ALL ON SCHEMA public TO ROOT; ALTER USER root WITH SUPERUSER;'", _become=True, _become_user="postgres")

    t.git(repo="https://github.com/outline/outline", dest="/root/outline")
    t.command(cmd="yarn install --frozen-lockfile", chdir="/root/outline")
    t.command(cmd="yarn build", chdir="/root/outline")

    t.template(dest="/root/outline/.env", src="env")


    t.copy(dest="/etc/systemd/system/outline.service",
        content=dedent("""
        [Unit]
        Wants=networking.target

        [Service]
        ExecStart=/usr/bin/env yarn start
        WorkingDirectory=/root/outline
        Restart=always
        RestartSec=30
        User=root

        [Install]
        WantedBy=multi-user.target
        """))
    t.systemd(daemon_reload=True)
    t.service(name="outline", state="restarted", enabled=True)
