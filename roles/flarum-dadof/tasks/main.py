from pyplays import Tasks

with Tasks() as t:
    t.package(
        name=["httpd", "mariadb-server", "php", "php-mbstring", "php-xml",
              "php-curl", "php-gd", "php-zip", "php-pdo", "php-mysqlnd",
              "git", "unzip"],
        state="latest")
    t.file(path="/ssd_data/mariadb", state="directory", owner="mysql", mode=0o750)
    t.copy(dest="/etc/my.cnf.d/99-mariadb-server-data-location.cnf",
        content="""
        [mysqld]
        datadir=/ssd_data/mariadb
        """)
    t.service(name="mariadb", state="restarted", enabled=True)

    t.file(path="/srv/flarum", state="directory")
    t.file(path="/srv/flarum/storage", state="directory", owner="apache")
    for d in ["cache", "formatter", "less", "locale", "logs", "sessions", "tmp", "views"]:
        t.file(path=f"/srv/flarum/storage/{d}", state="directory", owner="apache")
    t.file(path="/srv/flarum/public", state="directory")
    t.file(path="/srv/flarum/public/assets", state="directory", owner="apache")

    # For fof/upload extension
    t.file(path="/ssd_data/flarum_files", state="directory", owner="apache", mode=0o750)
    t.file(path="/srv/flarum/public/assets/files", state="link",
        src="/ssd_data/flarum_files")

    t.get_url(
        url="https://getcomposer.org/installer",
        dest="/srv/flarum/composer-installer.php")
    t.command(cmd="php composer-installer.php", chdir="/srv/flarum")
    t.file(path="/srv/flarum/composer-installer.php", state="absent")

    t.get_url(
        url="https://github.com/flarum/flarum/raw/master/flarum",
        dest="/srv/flarum/flarum",
        mode=0o755)
    t.get_url(
        url="https://github.com/flarum/flarum/raw/master/extend.php",
        dest="/srv/flarum/extend.php")
    t.get_url(
        url="https://github.com/flarum/flarum/raw/master/site.php",
        dest="/srv/flarum/site.php")
    t.get_url(
        url="https://github.com/flarum/flarum/raw/master/public/.htaccess",
        dest="/srv/flarum/public/.htaccess")
    t.get_url(
        url="https://github.com/flarum/flarum/raw/master/public/index.php",
        dest="/srv/flarum/public/index.php")
    t.copy(src="composer.json", dest="/srv/flarum/composer.json",
        _tags="flarum-dadof-composer")
    t.command(cmd="php composer.phar install", chdir="/srv/flarum",
        _tags="flarum-dadof-composer")

    t.copy(dest="/etc/httpd/conf/httpd.conf",
        content="""
        ServerRoot "/etc/httpd"
        Listen 80
        User apache
        Group apache
        TypesConfig /etc/mime.types

        Include conf.modules.d/*.conf

        <Directory />
            Require all denied
        </Directory>

        DocumentRoot "/srv/flarum/public"
        <Directory "/srv/flarum/public">
            AllowOverride All
            Require all granted
            Options FollowSymLinks
        </Directory>

        IncludeOptional conf.d/*.conf
        """)
    t.service(name="httpd", state="restarted", enabled="yes")

    # Database setup is manual:
    # CREATE DATABASE flarum;
    # CREATE USER flarum IDENTIFIED BY '<PW>';
    # GRANT ALL ON flarum.* TO flarum;

    # Flarum setup is also manual, chown apache /srv/flarum for config.php
    # creation, then back to root for security
