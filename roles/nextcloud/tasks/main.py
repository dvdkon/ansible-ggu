from textwrap import dedent
from pyplays import Tasks

nextcloud_version = "30.0.2"

with Tasks() as t:
    t.command("Enable CodeReadyBuilder repo", "crb enable")
    t.dnf(name="epel-release", state="installed")
    t.stat(_register="remi_stat", path="/etc/yum.repos.d/remi.repo")
    t.dnf(name="https://rpms.remirepo.net/enterprise/remi-release-9.rpm",
          state="installed",
          disable_gpg_check=True, # This package will import the GPG key
          _when="not remi_stat.stat.exists")
    t.command(cmd="dnf module switch-to -y php:remi-8.3")

    t.package(
        name=["git", "php", "php-gd", "php-xml", "php-mbstring", "php-ldap",
              "php-pecl-apcu", "php-opcache", "php-pecl-zip", "php-mysqlnd",
              "php-process", "php-intl", "php-sodium", "php-gmp", "php-bcmath",
              "php-pecl-imagick", "httpd", "mariadb-server", "make", "npm",
              "composer"],
        state="latest")

    t.file(path="/ssd_data", mode=0o755)
    t.file(path="/ssd_data/mariadb", state="directory", mode=0o700, owner="mysql")

    # XXX: Initialising the database in /ssd_data/mariadb is manual!
    # sudo -umysql mysql_install_db --auth-root-socket-user=mysql --basedir=/usr --datadir=/ssd_data/mariadb
    # CREATE USER apache@localhost IDENTIFIED VIA unix_socket;
    # CREATE DATABASE nextcloud;
    # GRANT ALL ON nextcloud.* TO apache@localhost;
    t.copy(dest="/etc/my.cnf",
        content=dedent("""
        [mariadb]
        datadir = /ssd_data/mariadb

        [client]
        default-character-set = utf8mb4

        [mysqld]
        bind_address = 127.0.0.1
        port = 3306
        socket = /var/lib/mysql/mysql.sock
        character_set_server = utf8mb4
        collation_server = utf8mb4_general_ci
        """))
    t.service(name="mariadb", state="restarted", enabled=True)

    t.copy(dest="/etc/httpd/conf/httpd.conf",
        content=dedent("""
        ServerRoot "/etc/httpd"
        Listen 80
        User apache
        Group apache
        TypesConfig /etc/mime.types

        Include conf.modules.d/*.conf

        <Directory />
            Require all denied
        </Directory>

        DocumentRoot "/srv/nextcloud"
        <Directory "/srv/nextcloud">
            AllowOverride All
            Require all granted
            Options FollowSymLinks
        </Directory>

        IncludeOptional conf.d/*.conf
        """))
    t.copy(dest="/etc/php.d/99-mem.ini",
        content=dedent("""
        [PHP]
        memory_limit=1G
        """))

    t.service(name="httpd", state="stopped")

    t.git(
        repo="https://github.com/nextcloud/server.git",
        version="v" + nextcloud_version,
        dest="/srv/nextcloud",
        force=True,
        depth=1)
    t.git(
        repo="https://github.com/nextcloud/viewer.git",
        version="v" + nextcloud_version,
        dest="/srv/nextcloud/apps/viewer",
        force=True)

    t.git(
        repo="https://github.com/nextcloud/photos.git",
        version="v" + nextcloud_version,
        dest="/srv/nextcloud/apps/photos",
        force=True)
    t.command(
        cmd="make npm-init composer-install build-js-production",
        chdir="/srv/nextcloud/apps/photos")

    t.file(path="/data/nextcloud_data", state="directory", owner="apache", mode=0o700)
    t.file(path="/ssd_data/nextcloud_config", state="directory", owner="apache", mode=0o700)
    t.file(path="/srv/nextcloud/config", state="absent")
    t.file(path="/ssd_data/nextcloud_config/CAN_INSTALL", state="absent")
    t.file(src="/ssd_data/nextcloud_config", dest="/srv/nextcloud/config", state="link")
    t.file(path="/srv/nextcloud/apps", owner="apache")

    t.copy(dest="/srv/nextcloud/config/.htaccess",
        content=dedent("""
        Order Allow,Deny
        Deny from all
        Satisfy All
        """))
    t.shell(_register="nc_version",
            # TODO: Support in pyplays
            #_ignore_errors=True,
            cmd="""sed -nEe "s/.*'version' => '(.*)',.*/\\1/p" /srv/nextcloud/config/config.php""")
    t.copy(dest="/srv/nextcloud/config/config.php",
        owner="apache", mode=0o640,
        content=dedent("""<?php
        $CONFIG = array (
          'instanceid' => 'oc6csbgq850w',
          'passwordsalt' => '{{ lookup('ini', 'passwordsalt file=secrets/passwords.ini section=nextcloud') }}',
          'secret' => '{{ lookup('ini', 'secret file=secrets/passwords.ini section=nextcloud') }}',
          'trusted_domains' => array ( 0 => 'nextcloud.ggu.cz', ),
          'datadirectory' => '/data/nextcloud_data',
          'dbtype' => 'mysql',
          'version' => '{% if nc_version.rc == 0 %}{{ nc_version.stdout }}{% endif %}',
          'overwrite.cli.url' => 'https://nextcloud.ggu.cz',
          'htaccess.RewriteBase' => '/',
          'overwriteprotocol' => 'https',
          'dbname' => 'nextcloud',
          'dbhost' => 'localhost:/var/lib/mysql/mysql.sock',
          'dbport' => '',
          'dbtableprefix' => 'oc_',
          'mysql.utf8mb4' => true,
          'dbuser' => 'apache',
          'dbpassword' => 'M2!CID@_#Js8t1dLkcMn|-2@YI.32j',
          'installed' => true,
          'ldapProviderFactory' => 'OCA\\User_LDAP\\LDAPProviderFactory',
          'memcache.local' => '\OC\Memcache\APCu',
          'default_phone_region' => 'CZ',
        );
        """))

    t.command(argv=["php", "--define", "apc.enable_cli=1",
                    "/srv/nextcloud/occ", "upgrade"],
        _become=True, _become_user="apache")

    t.file(path="/srv/nextcloud/.htaccess", owner="apache")
    t.command(argv=["php", "--define", "apc.enable_cli=1",
                    "/srv/nextcloud/occ", "maintenance:update:htaccess"],
        _become=True, _become_user="apache")
    t.service(name="httpd", state="started", enabled="yes")

    t.copy(dest="/etc/systemd/system/nextcloud-cron.service",
        content=dedent("""
        [Service]
        ExecStart=/usr/bin/env php --define apc.enable_cli=1 -f /srv/nextcloud/cron.php
        User=apache
        WorkingDirectory=/srv/nextcloud
        KillMode=process
        """))
    t.copy(dest="/etc/systemd/system/nextcloud-cron.timer",
        content=dedent("""
        [Timer]
        Unit=nextcloud-cron.service
        OnBootSec=5min
        OnUnitActiveSec=5min

        [Install]
        WantedBy=timers.target
        """))

    t.systemd(daemon_reload=True)
    t.service(name="nextcloud-cron.timer", state="started", enabled=True)

