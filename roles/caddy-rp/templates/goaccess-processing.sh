#!/bin/bash
set -e

log_directory="/data/access_logs"
output_directory="/srv/analytics"
salt={{ lookup("ini", "goaccess_salt file=secrets/passwords.ini section=caddy-rp") }}

# Loop through each log file in the specified directory
for log_file in "$log_directory"/*.access; do
  
  # Get the filename without extension
  filename=$(basename -- "$log_file")
  filename_no_extension="${filename%.*}"
  url_direcotry=$filename_no_extension-$(sha256sum <<< ${salt}$filename_no_extension | head -c 64)

  echo "Creating direcotry $url_direcotry"
  mkdir -p "$output_directory/$url_direcotry"

  echo "generating report for $filename"

  # Generate HTML report using goaccess
  goaccess --log-format=caddy \
	  --html-report-title="$filename_no_extension" \
	  --tz=Europe/Prague \
  	  --agent-list \
	  --with-output-resolver \
	  --ignore-crawlers \
	  --real-os \
	  --no-ip-validation \
	  --log-format='{ "ts": "%x.%^", "request": { "remote_ip": "%h", "proto":"%H", "method": "%m", "host": "%v", "uri": "%U" }, "duration": "%T", "size": "%b","status": "%s" }' \
	  --http-protocol=no \
	  --http-method=no \
	  --time-format="%s" \
	  --date-format="%s" \
	  --log-file="$log_file" \
	  --geoip-database=/var/geoip.mmdb \
	  --output="$output_directory/$url_direcotry/index.html"

  echo "HTML report generated for $filename ( $output_directory/$url_direcotry/index.html )"
done

echo "All HTML reports generated successfully."

