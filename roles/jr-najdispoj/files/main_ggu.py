import logging

logging.root.handlers = []
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s.%(msecs)03d - %(levelname)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)

from server.init import geocoding_service, routing_service, app
from server.providers.geocoding.opentripplanner import OpenTripPlannerGeocodingProvider
from server.providers.geocoding.otp_xapian import OtpXapianGeocodingProvider
from server.providers.routing.opentripplanner import OpenTripPlannerRoutingProvider
from server.providers.geocoding.photon import PhotonProvider

OTP_ADDRESS = "https://api.otp.jr.ggu.cz"

geocoding_service.register_provider(
    "OtpXapian",
    OtpXapianGeocodingProvider(OTP_ADDRESS),
)
geocoding_service.register_provider(
    "Photon", PhotonProvider("https://photon.komoot.io")
)

routing_service.register_provider(
    "OTP2",
    OpenTripPlannerRoutingProvider(OTP_ADDRESS, is_v2=True),
)
