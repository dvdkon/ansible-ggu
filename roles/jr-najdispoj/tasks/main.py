from textwrap import dedent
from pyplays import Tasks

with Tasks() as t:
    t.package(
        name=["python3", "python3-dev", "python3-venv", "libxapian-dev", "git",
            "nodejs", "node-npm", "caddy"],
        state="latest")
    t.git(
        repo="https://gitlab.com/dvdkon/najdispoj.git",
        dest="/opt/najdispoj",
        force=True)

    t.stat(path="/opt/najdispoj/venv", _register="venv_stat")
    t.command(cmd="python3 -m venv /opt/najdispoj/venv",
        _when="not venv_stat.stat.exists")

    t.shell(cmd="""
        . /opt/najdispoj/venv/bin/activate
        # See https://github.com/yaml/pyyaml/issues/724
        pip install PyYAML==5.3.1
        # Xapian prerequisites (see https://github.com/ninemoreminutes/xapian-bindings/issues/10)
        pip install wheel sphinxcontrib-applehelp==1.0.2 \\
            sphinxcontrib-serializinghtml sphinxcontrib-devhelp \\
            sphinxcontrib-htmlhelp sphinxcontrib-jsmath sphinxcontrib-qthelp
        pip install xapian-bindings
        pip install -r /opt/najdispoj/requirements.txt
    """)

    t.copy(src="main_ggu.py", dest="/opt/najdispoj/server/main_ggu.py")
    t.copy(src="config.py", dest="/opt/najdispoj/server/config.py")
    t.copy(src="config.ts", dest="/opt/najdispoj/client/config.ts")

    t.shell(chdir="/opt/najdispoj/client",
        cmd="""
        npm install
        npm run build
        """)

    t.copy(dest="/etc/caddy/Caddyfile",
        content=dedent("""
        :80 {
            root * /opt/najdispoj/client/dist
            file_server
            reverse_proxy /api/* localhost:8081
        }
        """))

    t.copy(dest="/etc/systemd/system/najdispoj-server.service",
        content=dedent("""
        [Unit]
        After=network.target

        [Service]
        ExecStart=/opt/najdispoj/venv/bin/uvicorn \\
            server.main_ggu:app \\
            --port 8081
        WorkingDirectory=/opt/najdispoj

        [Install]
        WantedBy=multi-user.target
        """))
    t.systemd(daemon_reload=True)
    t.service(name="najdispoj-server.service", state="restarted", enabled=True)
    t.service(name="caddy", state="reloaded")
