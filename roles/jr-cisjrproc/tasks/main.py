from textwrap import dedent
from pyplays import Tasks

with Tasks() as t:
    t.get_url(
        url="https://packages.microsoft.com/config/debian/12/packages-microsoft-prod.deb",
        dest="/root/microsoft.deb")
    t.apt(deb="/root/microsoft.deb")
    t.apt(update_cache=True)

    t.package(
        name=["dotnet-sdk-8.0", "git", "git-lfs", "curl", "wget", "caddy",
              "zip", "unzip", "lftp"],
        state="latest")

    t.file(path="/data/wwwroot/results", state="directory")
    t.file(path="/ssd_data/workdir", state="directory")
    t.copy(dest="/etc/caddy/Caddyfile",
        content=dedent("""
        :80

        root * /data/wwwroot
        file_server {
            browse
        }
        encode zstd gzip
        """))
    t.service(name="caddy", state="reloaded")

    t.command(cmd="git lfs install")
    t.git(
        repo="https://gitlab.com/dvdkon/jrutil.git",
        dest="/opt/jrutil",
        version="cisjrproc-prod")
    t.command(cmd="dotnet build jrutil-multitool -c Release",
        chdir="/opt/jrutil")

    t.git(
        repo="https://gitlab.com/dvdkon/jrunify-ext-geodata.git",
        dest="/opt/ext-geodata")

    t.copy(dest="/usr/local/bin/process-cisjr.sh", mode=0o755,
        content="""#!/usr/bin/env bash
mkdir_empty() {
    [ -e $1 ] && rm -r $1
    mkdir $1
}

function log() {
    echo "$(date -Iseconds)" "$@"
}

outdir_finished=/data/wwwroot/results/$(date -Idate)
outdir=$outdir_finished.unfinished
latest_dir=/data/wwwroot/results/latest
mkdir -p $outdir

log Forwarding log to $outdir/main.log
exec >$outdir/main.log 2>&1

export JRUTIL_LOG_TO_CONSOLE=0

multitool=$(ls -t /opt/jrutil/jrutil-multitool/bin/Release/*/jrutil-multitool | head -n1)

cd /ssd_data/workdir

log Downloading JDF data...
curl --time-cond JDF.zip -o JDF.zip ftp://ftp.cisjr.cz/JDF/JDF.zip
curl --time-cond JDF_mhd.zip -o JDF_mhd.zip ftp://ftp.cisjr.cz/draha/mestske/JDF.zip

log Downloading CZPTT data...
wget --no-parent --no-clobber --no-host-directories --cut-dirs=2 --recursive \\
    --quiet \\
    ftp://ftp.cisjr.cz/draha/celostatni/szdc

log Extracting JDF archives...
mkdir_empty JDF
mkdir_empty JDF_mhd
unzip -q -d JDF JDF.zip
unzip -q -d JDF_mhd JDF_mhd.zip

log Preparing geodata CSVs...
cat /opt/ext-geodata/other/* > other_geodata.csv

python3 <<'EOF'
import csv
import sys

w = csv.writer(open("rail_geodata.csv", "w"))
for l in csv.reader(open("/opt/ext-geodata/rail/SR70.csv")):
    # Strip checksum digit for compatibility with JrUtil IDs
    sr70 = l[0][:5]
    w.writerow((f"-SR70ST-CZ-{sr70}", l[2], l[3]))
EOF

log Fixing non-MHD JDF...
mkdir_empty JDF_fixed
$multitool fix-jdf JDF JDF_fixed \\
    --logfile rjson:$outdir/fixing.log.json \\
    --ext-geodata other_geodata.csv \\
    --cz-pbf /shareddata/osmdata/europe/czech-republic/data.pbf

log Fixing MHD JDF...
mkdir_empty JDF_mhd_fixed
$multitool fix-jdf JDF_mhd JDF_mhd_fixed \\
    --logfile rjson:$outdir/fixing.log.json \\
    --ext-geodata other_geodata.csv \\
    --cz-pbf /shareddata/osmdata/europe/czech-republic/data.pbf

log Merging JDF...
mkdir_empty JDF_merged
$multitool merge-jdf \\
    --logfile rjson:$outdir/merging.log.json \\
    JDF_merged JDF_fixed JDF_mhd_fixed

cd JDF_merged
zip $outdir/JDF_merged.zip *
cd ..

log Converting JDF to GTFS...
mkdir_empty JDF_merged_GTFS
$multitool jdf-to-gtfs \\
    --logfile rjson:$outdir/jdf-to-gtfs.log.json \\
    JDF_merged JDF_merged_GTFS

cd JDF_merged_GTFS
zip $outdir/JDF_merged_GTFS.zip *
cd ..

log Converting CZPTT XML to GTFS...
mkdir_empty CZPTT_GTFS
$multitool czptt-to-gtfs \\
    --logfile rjson:$outdir/czptt-to-gtfs.log.json \\
    --stop-coords-by-id=rail_geodata.csv \\
    szdc CZPTT_GTFS

cd CZPTT_GTFS
zip $outdir/CZPTT_GTFS.zip *
cd ..

if [ -e $outdir_finished ]; then rm -rf $outdir_finished; fi
mv $outdir $outdir_finished
rm $latest_dir
ln -sf $outdir_finished $latest_dir
log "Finished!"
""")

    t.copy(dest="/etc/systemd/system/process-cisjr.service",
        content="""
[Unit]
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/process-cisjr.sh
SupplementaryGroups=shareddata
""")

    t.copy(dest="/etc/systemd/system/process-cisjr.timer",
        content="""
[Timer]
OnCalendar=*-*-* 22:00 Europe/Prague

[Install]
WantedBy=timers.target
""")

    t.systemd(daemon_reload=True)
    t.service(name="process-cisjr.timer", state="started", enabled=True)
