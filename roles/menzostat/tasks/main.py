from pyplays import Tasks
from textwrap import dedent

with Tasks() as t:
    t.task("community.general.timezone", name="Europe/Prague")
    # Gnuplot has to be installed manually, no package!
    t.package(name=["gcc", "g++", "ncurses", "gmp-devel", "zlib-devel",
                    "pcre-devel", "git", "postgresql-server", "libpq-devel"],
              state="latest")
    t.file(path="/ssd_data/postgresql", state="directory", owner="postgres")
    # XXX: Manually init PG database:
    #   postgresql-new-systemd-unit --unit=postgresql@menzostat --datadir=/ssd_data/postgresql/db
    #   postgresql-setup --initdb --unit=postgresql@menzostat --port=5432
    #   systemctl enable --now postgresql@menzostat
    #   sudo -iu postgres psql -c 'CREATE USER root; CREATE DATABASE menzostat'

    t.stat(path="/root/.ghcup/env", _register="ghcup_env")
    t.shell(cmd="""
        export BOOTSTRAP_HASKELL_NONINTERACTIVE=1
        export BOOTSTRAP_HASKELL_GHC_VERSION=9.2.4
        export BOOTSTRAP_HASKELL_CABAL_VERSION=latest
        export BOOTSTRAP_HASKELL_INSTALL_STACK=0
        export BOOTSTRAP_HASKELL_INSTALL_HLS=0
        export BOOTSTRAP_HASKELL_ADJUST_BASHRC=P
        curl -sSf https://get-ghcup.haskell.org | sh
        """,
        _when="not ghcup_env.stat.exists")
    t.git(
        repo="https://gitlab.com/dvdkon/menzostat",
        dest="/root/menzostat",
        version="prod")
    t.shell(cmd="source /root/.ghcup/env && cabal update")
    t.shell(cmd="source /root/.ghcup/env && cabal install --overwrite-policy=always",
        chdir="/root/menzostat")

    t.file(path="/data/photos", state="directory")

    t.copy(dest="/etc/systemd/system/menzostat-scraper.service",
        content=dedent("""
        [Unit]
        Wants=networking.target

        [Service]
        ExecStart=/root/.cabal/bin/menzostat \
            -d "host=/var/run/postgresql/ dbname=menzostat" \
            run-scraper
        WorkingDirectory=/root/menzostat
        Restart=always
        RestartSec=30

        [Install]
        WantedBy=multi-user.target
        """))
    t.copy(dest="/etc/systemd/system/menzostat-worker.service",
        content=dedent("""
        [Service]
        ExecStart=/root/.cabal/bin/menzostat \
            -d "host=/var/run/postgresql/ dbname=menzostat" \
            run-worker 
        WorkingDirectory=/root/menzostat
        """))
    t.copy(dest="/etc/systemd/system/menzostat-worker.timer",
        content=dedent("""
        [Timer]
        Unit=menzostat-worker.service
        OnCalendar=*-*-* *:00

        [Install]
        WantedBy=timers.target
        """))
    t.copy(dest="/etc/systemd/system/menzostat-photo-scraper.service",
        content="""
        [Service]
        ExecStart=/root/.cabal/bin/menzostat \
            -d "host=/var/run/postgresql/ dbname=menzostat" \
            run-photo-scraper \
            -P /data/photos/ \
            -u https://labk10.karlin.mff.cuni.cz/~konarid/neso.php/
        # The trailing slash is necessary!
        WorkingDirectory=/root/menzostat
        """)
    t.copy(dest="/etc/systemd/system/menzostat-photo-scraper.timer",
        content=dedent("""
        [Timer]
        Unit=menzostat-photo-scraper.service
        OnCalendar=*-*-* 10:50
        OnCalendar=*-*-* 11:20
        OnCalendar=*-*-* 11:50
        OnCalendar=*-*-* 12:50
        OnCalendar=*-*-* 13:50

        [Install]
        WantedBy=timers.target
        """))
    t.copy(dest="/etc/systemd/system/menzostat-webserver.service",
        content=dedent("""
        [Unit]
        Wants=networking.target

        [Service]
        ExecStart=/root/.cabal/bin/menzostat \
            -d "host=/var/run/postgresql/ dbname=menzostat" \
            http-server \
            -p 80 \
            -P /data/photos/
        WorkingDirectory=/root/menzostat
        Restart=always
        RestartSec=30

        [Install]
        WantedBy=multi-user.target
        """))
    t.systemd(daemon_reload=True)
    t.service(name="menzostat-scraper", state="restarted", enabled=True)
    t.service(name="menzostat-worker.timer", state="restarted", enabled=True)
    t.service(name="menzostat-photo-scraper.timer", state="restarted", enabled=True)
    t.service(name="menzostat-webserver", state="restarted", enabled=True)
