from pyplays import Tasks

goatcounter_version = "2.5.0"

with Tasks() as t:
    t.get_url("Download goatcounter binary",
        url=f"https://github.com/arp242/goatcounter/releases/download/v{goatcounter_version}/goatcounter-v{goatcounter_version}-linux-amd64.gz",
        dest="/root/goatcounter.gz")
    t.command("Extract goatcounter",
        "gunzip goatcounter.gz")
    t.copy(remote_src=True,
        src="/root/goatcounter",
        dest="/usr/local/bin/goatcounter",
        mode=0o755)
    t.file(path="/root/goatcounter", state="absent")
    t.file(path="/root/goatcounter.gz", state="absent")

    t.file(path="/ssd_data/goatcounter", state="directory")

    t.copy(
        dest="/etc/systemd/system/goatcounter.service",
        content="""
        [Unit]
        Description=Lightweight web analytics

        [Service]
        Type=simple
        ExecStart=/usr/local/bin/goatcounter serve \
            -db sqlite+/ssd_data/goatcounter/db.sqlite3 \
            -listen *:80 \
            -tls http \
            -automigrate

        [Install]
        WantedBy=multi-user.target
        """)
    t.systemd(daemon_reload=True)
    t.service(name="goatcounter", state="restarted", enabled=True)
