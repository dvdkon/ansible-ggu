#!/usr/bin/env sh
# Switch to full speed fan mode
ipmitool raw 0x30 0x45 0x01 0x01
# Set fan limits low enough
for fan in "FAN 1" "FAN 2" "FAN 3" "FAN 4" "FAN A"; do
	ipmitool sensor thresh "$fan" lower 100 100 100
done
# Check that SuperIO chip is hwmon2
hwmon=/sys/class/hwmon/hwmon2
if [ $(cat $hwmon/name) != nct6776 ]; then
	echo Unknown hwmon: $(cat $hwmon/name) >&2
	exit 1
fi
# Enable fan manual control by talking to the SuperIO chip directly
# pwm1 doesn't seem to do much
echo 1 > $hwmon/pwm1_enable
echo 1 > $hwmon/pwm2_enable
# Set fan speed to roughly 800 RPM
echo 80 > $hwmon/pwm1
echo 80 > $hwmon/pwm2
