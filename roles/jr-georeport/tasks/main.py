from textwrap import dedent
from pyplays import Tasks

with Tasks() as t:
    t.get_url(
        url="https://packages.microsoft.com/config/debian/12/packages-microsoft-prod.deb",
        dest="/root/microsoft.deb")
    t.apt(deb="/root/microsoft.deb")
    t.apt(update_cache=True)

    t.package(name=["dotnet-sdk-6.0", "git", "caddy", "unzip", "lftp"], state="latest")

    t.file(path="/srv/georeport", state="directory")
    t.copy(dest="/etc/caddy/Caddyfile",
        content=dedent("""
        :80

        root * /srv/georeport
        file_server
        encode zstd gzip
        """))

    t.git(
        repo="https://gitlab.com/dvdkon/jrutil.git",
        dest="/opt/jrutil",
        version="georeport-prod")
    t.command(cmd="dotnet build georeport -c Release",
        chdir="/opt/jrutil")

    t.git(
        repo="https://gitlab.com/dvdkon/jrunify-ext-geodata.git",
        dest="/opt/ext-geodata")

    t.file(path="/ssd_data/cisjr", state="directory")
    t.copy(dest="/usr/local/bin/georeport.sh", mode=0o755,
        content="""#!/usr/bin/env bash
cd /ssd_data/cisjr

echo Mirroring ftp.cisjr.cz...
lftp ftp://ftp.cisjr.cz/ <<EOF
connect
mirror -n JDF
mirror -n draha/mestske
mirror -n draha/celostatni/szdc
EOF

echo Unzipping JDF files...
[ -e JDF_ex ] && rm -r JDF_ex
mkdir -p JDF_ex/main JDF_ex/mhd
unzip -d JDF_ex/main JDF/JDF.zip
unzip -d JDF_ex/mhd mestske/JDF.zip

georeport() { dotnet exec /opt/jrutil/georeport/bin/Release/*/georeport.dll "$@"; }
echo Computing JDF stop list...
georeport jdf-to-stop-names JDF_ex > jdf_stop_names.txt
echo Computing CZPTT stop list...
georeport czptt-to-stop-list szdc > czptt_stop_names.csv
echo Creating report...
georeport report \\
    --rail-stops=czptt_stop_names.csv \\
    --other-stops=jdf_stop_names.txt \\
    --rail-ext-sources=/opt/ext-geodata/rail \\
    --other-ext-sources=/opt/ext-geodata/other \\
    --logfile=json:/srv/georeport/log.txt \\
    --cache-dir= \\
    > /srv/georeport/index2.html
mv /srv/georeport/index2.html /srv/georeport/index.html
echo Report finished!
        """)
    t.copy(dest="/etc/systemd/system/georeport.service",
        content="""
        [Unit]
        After=network.target

        [Service]
        Type=oneshot
        ExecStart=/usr/local/bin/georeport.sh
        """)
    t.copy(dest="/etc/systemd/system/georeport.timer",
        content="""
        [Timer]
        OnCalendar=*-*-* 02:00

        [Install]
        WantedBy=timers.target
        """)
    t.systemd(daemon_reload=True)
    t.service(name="georeport.timer", state="started", enabled=True)
