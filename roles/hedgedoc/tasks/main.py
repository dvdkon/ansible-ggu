from pyplays import Tasks

hedgedoc_version = "1.9.3"

with Tasks() as t:
    t.dnf(name=["git", "tar", "nodejs", "gcc", "g++", "make"])
    # Corepack has disappeared (?!), so just use npm
    t.command("Install Yarn", argv=["npm", "--location=global", "install", "yarn"])
    t.file(path="/root/hedgedoc", state="directory")
    t.unarchive(
        remote_src=True,
        src=f"https://github.com/hedgedoc/hedgedoc/releases/download/{hedgedoc_version}/hedgedoc-{hedgedoc_version}.tar.gz",
        dest="/root/hedgedoc")
    t.command("Is Yarn classic already?",
        argv=["yarn", "--version"],
        _register="yarn_version")
    t.command("Use classic Yarn",
        argv=["yarn", "set", "version", "classic"],
        chdir="/root/hedgedoc",
        _when="not yarn_version.stdout.startswith('1.')")
    t.command("Build Hedgedoc",
        argv=["bin/setup"],
        chdir="/root/hedgedoc/hedgedoc")
    t.template(
        src="config.json.j2",
        dest="/root/hedgedoc/hedgedoc/config.json")
    t.copy(
        dest="/etc/systemd/system/hedgedoc.service",
        content="""
        [Unit]
        Description=HedgeDoc collaborative Markdown editor

        [Service]
        ExecStart=/usr/bin/env yarn start --production
        Environment=NODE_ENV=production
        WorkingDirectory=/root/hedgedoc/hedgedoc
        Restart=always

        [Install]
        WantedBy=multi-user.target
        """)
    t.systemd(daemon_reload=True)
    t.service(name="hedgedoc", state="restarted", enabled=True)
