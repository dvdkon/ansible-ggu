from textwrap import dedent
from pyplays import Tasks, Args

args = Args(vars, ["osm_regions"])

with Tasks() as t:
    t.package(name="pyosmium", state="latest")

    script = "#!/bin/sh\n"
    for region in args.osm_regions:
        dir = f"/shareddata/osmdata/{region}"
        t.file(path=dir, state="directory")
        t.stat(path=f"{dir}/data.pbf", _register="stat")
        t.get_url(
            url=f"https://download.geofabrik.de/{region}-latest.osm.pbf",
            dest=f"{dir}/data.pbf",
            _when="not stat.stat.exists")
        script += dedent(f"""
            """)

    t.copy(dest="/usr/local/bin/update-osm.sh", mode=0o755,
        content=dedent("""\
            #!/bin/sh
            find /shareddata/osmdata -name data.pbf | while read f; do
                echo Updating $f...
                pyosmium-up-to-date $f
                ret=$?
                if [ $ret != 0 ] && [ $ret != 3 ]; then echo "Failed! $ret"; fi
            done
            """))
    t.copy(dest="/etc/systemd/system/update-osm.service",
        content=dedent("""\
            [Service]
            Type=oneshot
            ExecStart=/usr/local/bin/update-osm.sh
            """))
    t.copy(dest="/etc/systemd/system/update-osm.timer",
        content=dedent("""\
            [Timer]
            OnCalendar=10:00

            [Install]
            WantedBy=timers.target
            """))
    t.systemd(daemon_reload=True)
    t.service(name="update-osm.timer", state="started", enabled=True)
