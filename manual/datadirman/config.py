DATA_DIR_ROOT = "/mnt/main-data/userdata"
LDAP_SERVER_URL = "ldap://10.80.0.8"
LDAP_BIND_DN = "uid=ldap-svc-datadirman,ou=users,dc=ggu,dc=cz"
LDAP_BASE = "dc=ggu,dc=cz"
PASSWORDS_INI = "../secrets/passwords.ini"

from configparser import ConfigParser
passwords_ini = ConfigParser()
passwords_ini.read(PASSWORDS_INI)
LDAP_BIND_PW = passwords_ini["ldap_sw"]["datadirman"]

# Groups where each user has write permissions
GROUPS_RW = {"uzu8", "ggu-media"}
# Groups where only the owner has write permissions
GROUPS_R = set()
# Other groups are ignored

ROOT_USER = "ldaproot"
ANONYMOUS_USER = "anonymous"
ANONYMOUS_WRITE_GROUP = "anonymous-rw"
