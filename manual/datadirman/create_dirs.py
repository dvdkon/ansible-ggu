#!/usr/bin/env python3
import pathlib
import os
import subprocess as sp
import ldap3
import datadirman.config as config

USER_DATA_DIR = pathlib.Path(config.DATA_DIR_ROOT) / "users"
GROUP_DATA_DIR = pathlib.Path(config.DATA_DIR_ROOT) / "groups"
ANONYMOUS_DIR = pathlib.Path(config.DATA_DIR_ROOT) / "anonymous"

def ldap_uid_for(ldap_conn, username):
    assert ldap_conn.search(
        config.LDAP_BASE,
        f"(&(objectClass=posixAccount)(uid={username}))",
        attributes=["uidNumber"])
    return int(next(iter(ldap_conn.entries)).uidNumber.value)

def ldap_gid_for(ldap_conn, groupname):
    assert ldap_conn.search(
        config.LDAP_BASE,
        f"(&(objectClass=posixGroup)(cn={groupname}))",
        attributes=["gidNumber"])
    return int(next(iter(ldap_conn.entries)).gidNumber.value)

def ldap_user_list(ldap_conn):
    assert ldap_conn.search(
            config.LDAP_BASE, "(objectClass=posixAccount)",
            attributes=["cn", "uidNumber", "gidNumber"])
    return ldap_conn.entries

def create_user_dirs(ldap_conn):
    for user in ldap_user_list(ldap_conn):
        user_dir = USER_DATA_DIR / user.cn.value
        if not user_dir.exists():
            user_dir.mkdir()
        # Group-writeable for easier SW configuration
        user_dir.chmod(0o770)
        os.chown(user_dir, int(user.uidNumber.value), int(user.gidNumber.value))

def ldap_group_list(ldap_conn):
    assert ldap_conn.search(
        config.LDAP_BASE, "(objectClass=posixGroup)",
        attributes=["cn", "gidNumber"])
    return ldap_conn.entries

def create_group_dirs(ldap_conn):
    ldaproot_uid = ldap_uid_for(ldap_conn, config.ROOT_USER)
    for group in ldap_group_list(ldap_conn):
        if group.cn.value not in config.GROUPS_RW | config.GROUPS_R: continue

        group_dir = GROUP_DATA_DIR / group.cn.value
        if not group_dir.exists():
            group_dir.mkdir()

        sp.run(["chown", "-R", f"{ldaproot_uid}:{group.gidNumber.value}", group_dir])
        sp.run(["find", group_dir, "-type", "d", "-exec", "chmod", "g+s", "{}", ";"])
        if group.cn.value in config.GROUPS_RW:
            sp.run(["chmod", "-R", "u=rwX,g=rwX,o=", group_dir])
            sp.run(["setfacl", "--recursive", "--default", "--modify", "g::rwX", group_dir])
        elif group.cn.value in config.GROUPS_R:
            sp.run(["chmod", "-R", "u=rwX,g=rX,o=", group_dir])
            sp.run(["setfacl", "--recursive", "--default", "--modify", "g::rX", group_dir])

def setup_anonymous(ldap_conn):
    ldaproot_uid = ldap_uid_for(ldap_conn, config.ROOT_USER)
    anon_uid = ldap_uid_for(ldap_conn, config.ANONYMOUS_USER)
    anon_rw_gid = ldap_gid_for(ldap_conn, config.ANONYMOUS_WRITE_GROUP)
    # Don't allow the anonymous user to access user/group directories at all
    sp.run(["setfacl", "--modify", f"u:{anon_uid}:0", GROUP_DATA_DIR])
    sp.run(["setfacl", "--modify", f"u:{anon_uid}:0", USER_DATA_DIR])

    ANONYMOUS_DIR.mkdir(exist_ok=True)
    os.chown(ANONYMOUS_DIR, ldaproot_uid, anon_rw_gid)
    # Allow the anonymous user/others to only access subfolders by knowing the
    # name (since they only have execute permissions but not read)
    # Members of anonymous_write can view subfolders and create new ones (TODO:
    # Should they be able to read?)
    os.chmod(ANONYMOUS_DIR, 0o0771)
    sp.run(["setfacl", "--modify", f"u:{anon_uid}:X", ANONYMOUS_DIR])
    sp.run(["setfacl", "--default", "--modify", f"u:{anon_uid}:rX", ANONYMOUS_DIR])

if __name__ == "__main__":
    ldap_conn = ldap3.Connection(
        config.LDAP_SERVER_URL, config.LDAP_BIND_DN, config.LDAP_BIND_PW,
        auto_bind=True)
    create_user_dirs(ldap_conn)
    create_group_dirs(ldap_conn)
    setup_anonymous(ldap_conn)
